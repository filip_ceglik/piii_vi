namespace PIII_VI
{
    public class Person
    {
        public int id;
        public string name, lastname;

        public Person(int id, string name, string lastname)
        {
            this.id = id;
            this.name = name;
            this.lastname = lastname;
        }
    }
}