﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using RandomDataGenerator.Randomizers;
using RandomDataGenerator.FieldOptions;

namespace PIII_VI
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> lst = Enumerable.Range(250, 125).ToList();

            List<int> byThree = lst.Where(x => x % 3 == 0).ToList();
            int onPage = 25;
            int pageNo = 2;
            List<int> page = lst.Skip(onPage * (pageNo - 1)).Take(onPage).ToList();
            double avg = lst.Average();
            /*foreach (var item in page)
            {
                Console.WriteLine(item);
            }*/

            //List<Person> people = Enumerable.Range(1, 50).Select(x => new Person() {id = x}).ToList();

            //List<Person> peopleQry = people.Where(x => x.id > 2).ToList();

            /*foreach (var item in peopleQry)
            {
                Console.WriteLine("Person no {0}, name: {1}, lastname: {2}", item.id, item.name,item.lastname);    //print people with id greater than 2
            }*/

            /*IEnumerable<int> peopleQry = people.Select(x => x.id);    //get all ids

            foreach (var item in peopleQry)
            {
                Console.WriteLine($"id: {item}");
            }*/

            /*people.Select(x => x.lastname).Distinct().ToList().ForEach(Console.WriteLine); //get distinct last names and print them
            //List<string> lastNames = people.Select(x => x.lastname).Distinct().ToList().ForEach(Console.WriteLine); // <= this won't work
            int first = lst.First(x => x % 300 == 0);
            int firstOrDef = lst.FirstOrDefault(x => x % 300 == 0);*/
            var intGenerator = RandomizerFactory.GetRandomizer(new FieldOptionsInteger());
            var nameGenerator = RandomizerFactory.GetRandomizer(new FieldOptionsFirstName());
            var lastNameGenerator = RandomizerFactory.GetRandomizer(new FieldOptionsLastName());
            
            Person rndPerson = new Person(id: intGenerator.Generate().Value, name: nameGenerator.Generate(),lastNameGenerator.Generate());
            Console.WriteLine("ID: {0}, Name: {1}, Last name: {2}", rndPerson.id,rndPerson.name,rndPerson.lastname);
            List<Person> somePeople = Enumerable.Range(1, 100).Select(x =>
                    new Person(intGenerator.Generate().Value, nameGenerator.Generate(), lastNameGenerator.Generate()))
                .ToList();

            #region Sorting
            
            somePeople.OrderBy(x => x.lastname).ToList().ForEach(x => Console.WriteLine("Sorted by last names: id {0}, name {1}, lastname {2}", x.id,x.name,x.lastname));
            somePeople.OrderBy(x => x.name).ToList().ForEach(x => Console.WriteLine("Sorted by names: id {0}, name {1}, lastname {2}", x.id,x.name,x.lastname));
            
            #endregion
        }
    }
}